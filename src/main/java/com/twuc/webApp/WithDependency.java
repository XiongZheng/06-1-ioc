package com.twuc.webApp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class WithDependency {
    private Dependent dependent;

    public Dependent getDependent() {
        return dependent;
    }

    public WithDependency(Dependent dependent) {
        this.dependent = dependent;
    }
}
