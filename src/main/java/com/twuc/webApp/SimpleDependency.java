package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SimpleDependency {
    private String name;

    public String getName() {
        return "O_o";
    }

    public void setName(String name) {
        this.name = name;
    }
}
