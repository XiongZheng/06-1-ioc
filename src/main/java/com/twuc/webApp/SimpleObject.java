package com.twuc.webApp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleObject implements SimpleInterface{

    SimpleDependency simpleDependency;

    @Bean
    public SimpleDependency getSimpleDependency() {
        simpleDependency.setName("O_o");
        return simpleDependency;
    }

    public  SimpleObject(SimpleDependency simpleDependency){
        this.simpleDependency = simpleDependency;
    }
}
