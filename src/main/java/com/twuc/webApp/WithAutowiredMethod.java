package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class WithAutowiredMethod {
    private Dependent dependent;
    private AnotherDependency anotherDependency;
    private ArrayList<String> log = new ArrayList<>();
    private ArrayList<Boolean> paramLog = new ArrayList<>();

    @Autowired
    public void Initialize(AnotherDependency anotherDependency) {
        this.anotherDependency = anotherDependency;
        log.add("anotherDependency");
        paramLog.add(anotherDependency == null);
    }

    public WithAutowiredMethod(Dependent dependent) {
        this.dependent = dependent;
        log.add("dependent");
        paramLog.add(dependent == null);
    }

    public Dependent getDependent() {
        return dependent;
    }

    public AnotherDependency getAnotherDependency() {
        return anotherDependency;
    }

    public ArrayList<String> getLog() {
        return log;
    }

    public ArrayList<Boolean> getParamLog() {
        return paramLog;
    }
}
