package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {

    private Dependent dependent;
    private String string;

    public MultipleConstructor(String string) {
        this.string = string;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public String getString() {
        return string;
    }

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }
}
