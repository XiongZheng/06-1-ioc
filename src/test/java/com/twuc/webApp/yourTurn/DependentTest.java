package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class DependentTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    //2.1-1
    @Test
    void should_get_object() {
        WithoutDependency bean = context.getBean(WithoutDependency.class);
        assertNotNull(bean);

        AnnotationConfigApplicationContext context2 = new AnnotationConfigApplicationContext("com.twuc.webApp");
        WithoutDependency bean2 = context2.getBean(WithoutDependency.class);
        assertNotEquals(bean,bean2);
    }

    //2.1-2
    @Test
    void should_get_dependency_object() {
        WithDependency bean = context.getBean(WithDependency.class);
        assertNotNull(bean);
        assertNotNull(bean.getDependent());
    }

    //2.1-3
    @Test
    void should_out_of_scope() {
        AnnotationConfigApplicationContext content = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
        assertThrows(RuntimeException.class,() -> {content.getBean(OutOfScanning.class);});
    }

    //2.1-4
    @Test
    void should_implements_interface() {
        Interface bean = context.getBean(Interface.class);
        assertNotNull(bean);
    }

    //2.1-5
    @Test
    void should_verify_simple_constructor() {
        SimpleObject simpleObject = (SimpleObject) context.getBean(SimpleInterface.class);
        SimpleDependency simpleDependency = simpleObject.getSimpleDependency();
        assertEquals("O_o", simpleDependency.getName());
    }

    //2.2-1
    @Test
    void should_test_constructor() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertNotNull(bean);
    }

    //2.2-2
    @Test
    void should_make_first_param_not_null() {
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);
        assertNotNull(bean.getDependent());
        assertNotNull(bean.getAnotherDependency());
        ArrayList<String> expect = new ArrayList<>();
        expect.add("dependent");
        expect.add("anotherDependency");
        assertIterableEquals(expect,bean.getLog());
        ArrayList<Boolean> expectParam = new ArrayList<>();
        expectParam.add(false);
        expectParam.add(false);
        assertIterableEquals(expectParam,bean.getParamLog());
    }

    //2.2-3
    @Test
    void should_implement_multiple_interfaces() {
        Map<String, InterfaceWithMultipleImpls> beans = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        Set<String> stringSet = beans.keySet();
        Object[] array = stringSet.toArray();
        assertEquals(3,array.length);
    }
}
